e-Commerce - CommWeb Payment Gateway

Module written by James Crook of Chocolate Chip multimedia, www.chocchipmm.com
Original release 23-12-2007

---

This modules allows payments to be made using the CommWeb (Commonwealth Bank Australia) payment gateway when using the e-Commerce module 4.x

You must already have the e-commerce module installed. Download the CommWeb module files and place them inside ecommerce's contrib folder like this: ecommerce/contrib/commweb. Enable the commweb gateway module and the settings will become available under receipt types. Enter your details as provided by your bank and enable payments for this receipt type.

Also note: CommWeb uses MIGS (MasterCard Internet Gateway Service) so you may be able to use or modify this module to work with other MIGS banking partners, eg Bendigo, ANZ etc.